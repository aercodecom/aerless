# aerLess Less Compiller. #


Instead of the old php based compiller, here is a node.js tool. It watches all the less files in the project, and in case of change, 
it generates the three front-end css: desktop.less.css, mobile.less.css and tablet.less.css

## Installation: ##

Install node.js on your computer, then download and unzip this tool. Then navigate its folder, and run the following command. 

`$ npm install`   // Linux based op.systems (ie. Ubuntu, OSX)
`npm install`   // DOS based op.systems (ie. Windows)

You will see a bunch of files in node_modules folder.

## Usage:

`$ node less.js /absolute/path/to/your/project/ [-c]`  // Linux based op.systems (ie. Ubuntu, OSX)
`node less.js /absolute/path/to/your/project/ [-c]`  // DOS based op.systems (ie. Windows)

 - 1st parameter [required]: absolute path to your project.
 - 2nd parameter [optional]: if -c compressed css output
