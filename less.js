var path = require('path');
var gaze = require('gaze');
var less = require('less');
var fs = require('fs');
var glob = require('glob');

if(typeof  process.argv[2] == "undefined"){
  console.log("Missing argument: absolute path of project forder");
  process.exit(1);
}

var siteRoot = process.argv[2];

if(!fs.existsSync(siteRoot)){
  console.log(siteRoot+" directory does not exists!");
  process.exit(1);
} 

console.log("Start to watch "+siteRoot+" directory for less files.");
var compression = false;
if(typeof  process.argv[3] != "undefined"){
  compression = true;
  console.log("CSS will be compressed.");
} else {
  console.log("CSS won't be compressed.");
}

var lessTypes = ["desktop.less","mobile.less","tablet.less"];

var buildLess = function(lessType){
  var lessString = "";
  glob(siteRoot+"/**/"+lessType, function (er, files) {

    if(files.length){
      console.log("Start to build "+lessType);
      for(var file in files){
        console.log("Reading "+files[file]);
        lessString += fs.readFileSync(files[file], "utf8");  
      }

      var parser = new(less.Parser)({
        paths: [siteRoot]
      });

      parser.parse(lessString, function(e, tree){
        if(e){
          console.log(e);
        }
        else{
          try{
            fs.writeFileSync(siteRoot+"/layout/css/"+lessType+".css",tree.toCSS({
              compress: compression
            }));
            console.log(lessType+".css has been generated!");
          }catch (ex) {
            console.log("Uh-oh: an error:");
            console.log(ex);
          }
        }
      });

    }else{
      console.log("Not any "+lessType+" was found!");
    }

  })
}
// Watch all .js files/dirs in process.cwd()
gaze(siteRoot+'/**/*.less', function(err, watcher) {
  this.on('changed', function(filepath) {
    console.log(filepath + ' was changed.');
    for(i in lessTypes){
      buildLess(lessTypes[i]);
    }
  });
});